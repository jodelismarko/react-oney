import axios from 'axios'

const COURSE_API_URL = 'http://localhost:4200'

class StoresDataService {

    async retrieveAllStores() {
        const response = await axios.get(`${COURSE_API_URL}/GetStoresResponse`);
        return response.data;
    }
}

export default new StoresDataService()