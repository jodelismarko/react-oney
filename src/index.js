import MaterialIcons from 'material-design-icons/iconfont/material-icons.css';
import * as serviceWorker from './serviceWorker';
import Lojas from './stores/lojas'
import './css/bootstrap.min.css';
import ReactDOM from 'react-dom';
import React from 'react';
import App from './main';


ReactDOM.render(
  <React.StrictMode>
    <Lojas />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
