import StoresDataService from '../service/storesDataService';
import React, { Component } from 'react'

class ListStores extends Component {
  constructor(props) {
    super(props)
    this.state = {
      stores: [],
    }
  }

  async componentDidMount() {
    const { StoreCrediboxList_Out } = await StoresDataService.retrieveAllStores();
    this.setState({ stores: StoreCrediboxList_Out })
  }
  render() {
    const data = this.state.stores;
    return (
      <div>
        <table className="table table-hover">
          <thead>
            <tr>
              <th scope="col">Name</th>
              <th scope="col">ShortName</th>
              <th scope="col">Code</th>
              <th scope="col">ParentGroupName</th>
              <th scope="col">ParentGroupCode</th>
              <th scope="col">Location</th>
              <th scope="col">HasInsurance</th>
              <th scope="col">HouseDateMandatory</th>
            </tr>
          </thead>
          <tbody>
            {data.map(repo => (
              <tr key={repo.ShortName}>
                <td >{repo.Name}</td>
                <td>{repo.ShortName}</td>
                <td>{repo.Code}</td>
                <td>{repo.ParentGroupName}</td>
                <td>{repo.ParentGroupCode}</td>
                <td>{repo.Location}</td>
                <td>{repo.HasInsurance}</td>
                <td>{repo.HouseDateMandatory}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    )
  }
}

export default ListStores
