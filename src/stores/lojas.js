import StoresDataService from '../service/storesDataService';
import MaterialTable from 'material-table';
import React, { Component } from 'react'
import { FormHelperText } from '@material-ui/core';

export default class Lojas extends Component {
    state = {
        selectedRow: null,
        stores: [],
        columns: [
            { title: 'Nome', field: 'Name' },
            { title: 'Short Name', field: 'ShortName' },
            { title: 'Codigo', field: 'Code' },
            { title: 'ParentGroupName', field: 'ParentGroupName' },
            { title: 'ParentGroupCode', field: 'ParentGroupCode' },
            { title: 'Location', field: 'Location' },
            { title: 'HasInsurance', field: 'HasInsurance' },
            { title: 'HouseDateMandatory', field: 'HouseDateMandatory' },
        ]
    }

    async componentDidMount() {
        const { StoreCrediboxList_Out } = await StoresDataService.retrieveAllStores();
        this.setState({ stores: StoreCrediboxList_Out })
    }

    render() {
        return (
            <MaterialTable
                columns={this.state.columns}
                data={this.state.stores}
                onRowClick={((evt, selectedRow) => this.setState({ selectedRow }))}

                options={{
                    showTitle: false,
                    pageSize: 20,
                    search: true ,
                    searchFieldAlignment: "left",
                    searchFieldStyle: {
                        width:"1620px",
                        height:"100%",
                        fontSize: "28px",
                    },
                    headerStyle: {
                        fontWeight: 'bold',
                        fontSize: 16,
                        color: '#333'
                    },
                    rowStyle: rowData => ({
                        backgroundColor: (this.state.selectedRow && this.state.selectedRow.tableData.id === rowData.tableData.id) ? '#EEE' : '#FFF'
                      })
                }}
            />
        )
    }
}
